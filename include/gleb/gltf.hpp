/**
 * @file glTF.hpp
 * @brief This file contains the declaration of the glTF class.
 */

#ifndef GLEB_GLTF_HPP
#define GLEB_GLTF_HPP
#include "accessor.hpp"
#include "animation.hpp"
#include "asset.hpp"
#include "buffer.hpp"
#include "bufferView.hpp"
#include "camera.hpp"
#include "image.hpp"
#include "internal/default_values.hpp"
#include "internal/valid_values.hpp"
#include "material.hpp"
#include "mesh.hpp"
#include "node.hpp"
#include "sampler.hpp"
#include "scene.hpp"
#include "skin.hpp"
#include "texture.hpp"

#include <rapidjson/document.h>

#include <fstream>
#include <iostream>
#include <memory>
#include <string>
#include <utility>
#include <vector>

namespace gleb {
/**
 * @brief The glTF class describes the root property of a glTF file.
 */
class glTF : public gltf_property {
public:
    explicit glTF(const std::string &source); //!< The default constructor.

    void read();

    std::vector<accessor>                 accessors;
    std::vector<animation>                animations;
    std::unique_ptr<asset_t>              asset = nullptr; // gltf_required
    std::vector<buffer>                   buffers;
    std::vector<bufferView>               bufferViews;
    std::vector<camera>                   cameras;
    std::vector<image>                    images;
    std::vector<material>                 materials;
    std::vector<mesh>                     meshes;
    std::vector<node>                     nodes;
    std::vector<animation_sampler>        samplers;
    std::optional<size_t>                 scene;
    std::vector<scene_t>                  scenes;
    std::vector<skin>                     skins;
    std::vector<std::shared_ptr<texture>> textures;

private:
    /**
     * @brief from_json parses the information from the glTF file.
     * @param doc is the JSON to be parsed.
     */
    void from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) override;

    void Clear() {
        // TODO: implement function to clear all members/set to default states
    }

    void ReadAccessors(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc);
    void ReadAnimations(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc);
    void ReadAsset(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc);
    void ReadBuffers(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc);
    void ReadBufferViews(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc);
    void ReadCameras(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc);
    void ReadImages(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc);
    void ReadMaterials(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc);
    void ReadMeshes(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc);
    void ReadNodes(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc);
    void ReadSamplers(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc);
    void ReadSceneDefault(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc);
    void ReadScenes(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc);
    void ReadSkins(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc);
    void ReadTextures(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc);
};
} // namespace gleb
#endif // GLEB_GLTF_HPP
