#define USE_GLEB_UTILS
#include <gleb/bufferView.hpp>

#include <gleb/internal/default_values.hpp>
#include <gleb/internal/utils.hpp>
#include <gleb/internal/valid_values.hpp>

using std::to_string;

namespace gleb {
using namespace rapidjson;

bufferView::bufferView(const std::string &source, const std::string &id) : gltf_property(source, id) {}

void bufferView::from_json(const GenericValue<UTF8<>> &doc) {
    AssertExistence(doc, "buffer", kNumberType);
    AssertExistence(doc, "byteLength", kNumberType);
    bool byteOffsetExists = check_existence(doc, "byteOffset", kNumberType);
    bool byteStrideExists = check_existence(doc, "byteStride", kNumberType);
    bool targetExists     = check_existence(doc, "target", kNumberType);
    bool nameExists       = check_existence(doc, "name", kStringType);

    buffer     = doc["buffer"].GetUint();
    byteLength = doc["byteLength"].GetUint();
    if (byteLength == 0) { ReportAndCrash("Byte length must be positive."); }
    if (byteOffsetExists) {
        byteOffset = doc["byteOffset"].GetUint();
    } else {
        byteOffset = default_byteOffset;
    }
    if (byteStrideExists) {
        byteStride = doc["byteStride"].GetUint();
        if (byteStride < minimum_byteStride || byteStride > maximum_byteStride) {
            ReportAndCrash("Byte stride has to be in the range of [4, 252].");
        }
    }
    if (targetExists) {
        target                                = doc["target"].GetUint();
        constexpr size_t ARRAY_BUFFER         = 34962;
        constexpr size_t ELEMENT_ARRAY_BUFFER = 34963;

        if (target != ARRAY_BUFFER && target != ELEMENT_ARRAY_BUFFER) {
            std::string msg = "bufferView target has to be either " + to_string(ARRAY_BUFFER) + "(ARRAY_BUFFER)" //
                              + " or " + to_string(ELEMENT_ARRAY_BUFFER) + "(ELEMENT_ARRAY_BUFFER).";
            FileReadError(Source, Identifier, msg);
        }
    }
    if (nameExists) { name = doc["name"].GetString(); }
    check_and_read_extensions(doc);
    check_and_read_extras(doc);
}
} // namespace gleb
#undef USE_GLEB_UTILS
