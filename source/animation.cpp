#define USE_GLEB_UTILS
#include <gleb/animation.hpp>

#include <gleb/internal/default_values.hpp>
#include <gleb/internal/utils.hpp>
#include <gleb/internal/valid_values.hpp>

namespace gleb {
using namespace rapidjson;
using namespace std;

animation::animation(const string &source, const string &id) : gltf_property(source, id) {}

void animation::from_json(const GenericValue<UTF8<>> &doc) {
    AssertExistence(doc, "channels", kArrayType);
    AssertExistence(doc, "samplers", kArrayType);

    channels.clear();
    for (SizeType i = 0; i < doc["channels"].GetArray().Size(); ++i) {
        animation_channel temp(Source, Identifier + ".channels[" + to_string(i) + "]");
        temp.from_json(doc["channels"][i]);
        channels.emplace_back(temp);
    }

    samplers.clear();
    for (SizeType i = 0; i < doc["samplers"].GetArray().Size(); ++i) {
        animation_sampler temp(Source, Identifier + ".samplers[" + to_string(i) + "]");
        temp.from_json(doc["samplers"][i]);
        samplers.emplace_back(temp);
    }

    if (check_existence(doc, "name", kStringType)) { name = doc["name"].GetString(); }

    check_and_read_extensions(doc);
    check_and_read_extras(doc);
}
} // namespace gleb
#undef USE_GLEB_UTILS
