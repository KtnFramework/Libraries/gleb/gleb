#define USE_GLEB_UTILS
#include <gleb/camera.hpp>

#include <gleb/internal/utils.hpp>
#include <gleb/internal/valid_values.hpp>

namespace gleb {
using namespace rapidjson;

camera::camera(const std::string &source, const std::string &id) : gltf_property(source, id) {}

void camera::from_json(const GenericValue<UTF8<>> &doc) {
    AssertExistence(doc, "type", kStringType);

    type = doc["type"].GetString();
    if (!check_validity(type, valid_camera_types)) { FileReadError(Source, Identifier, "Invalid camera type"); }

    if (check_existence(doc, "orthographic", kObjectType)) {
        orthographic.emplace(camera_orthographic(Source, Identifier + ".orthographic"));
        orthographic->from_json(doc["orthographic"]);
    }

    if (check_existence(doc, "perspective", kObjectType)) {
        perspective.emplace(camera_perspective(Source, Identifier + ".perspective"));
        perspective->from_json(doc["perspective"]);
    }

    if (check_existence(doc, "name", kStringType)) { name = doc["name"].GetString(); }

    check_and_read_extensions(doc);
    check_and_read_extras(doc);
}
} // namespace gleb
#undef USE_GLEB_UTILS
