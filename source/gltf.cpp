#define USE_GLEB_UTILS
#include <gleb/gltf.hpp>

#include <gleb/internal/utils.hpp>

namespace gleb {
using namespace rapidjson;
using namespace std;

glTF::glTF(const string &source) : gltf_property(source, "") {}

void glTF::from_json(const GenericValue<UTF8<>> &doc) {
    ReadAsset(doc); // must be first since it is the only required top level propertty of a valid gltf
    ReadAccessors(doc);
    ReadAnimations(doc);
    ReadBuffers(doc);
    ReadBufferViews(doc);
    ReadCameras(doc);
    ReadImages(doc);
    ReadMeshes(doc);
    ReadNodes(doc);
    ReadSamplers(doc);
    ReadSceneDefault(doc);
    ReadScenes(doc); // depends on nodes
    ReadSkins(doc);
    ReadTextures(doc);
    ReadMaterials(doc); // depends on textures
}

void glTF::read() {
    if (Source.empty()) { throw runtime_error(LocationReport(__FILE__, __LINE__, "m_Path is empty.")); }

    string   content;
    string   line;
    ifstream file;
    file.open(Source);
    if (!file.is_open()) { throw runtime_error(LocationReport(__FILE__, __LINE__, "Could not open " + Source + ".")); }
    while (getline(file, line)) { content += line; }
    Document document;
    document.Parse(content.c_str());
    from_json(document);
}

void glTF::ReadAccessors(const GenericValue<UTF8<>> &doc) {
    accessors.clear();
    const bool accessorsExists = check_existence(doc, "accessors", kArrayType);
    if (!accessorsExists) { return; }
    for (SizeType i = 0; i < doc["accessors"].GetArray().Size(); ++i) {
        accessor temp(Source, "accessors[" + to_string(i) + "]");
        temp.from_json(doc["accessors"][i]);
        accessors.emplace_back(move(temp));
    }
}

void glTF::ReadAnimations(const GenericValue<UTF8<>> &doc) {
    animations.clear();
    const bool accessorsExists = check_existence(doc, "animations", kArrayType);
    if (!accessorsExists) { return; }
    for (SizeType i = 0; i < doc["animations"].GetArray().Size(); ++i) {
        animation temp(Source, "animations[" + to_string(i) + "]");
        temp.from_json(doc["animations"][i]);
        animations.emplace_back(move(temp));
    }
}

void glTF::ReadAsset(const GenericValue<UTF8<>> &doc) {
    asset = make_unique<asset_t>(Source, "asset");
    if (!check_existence(doc, "asset", kObjectType)) { FileReadError(Source, "glTF", "Asset not found"); }
    asset->from_json(doc["asset"]);
}

void glTF::ReadBuffers(const GenericValue<UTF8<>> &doc) {
    buffers.clear();
    if (!check_existence(doc, "buffers", kArrayType)) { return; }
    for (SizeType i = 0; i < doc["buffers"].GetArray().Size(); ++i) {
        buffer temp(Source, "buffers[" + to_string(i) + "]");
        temp.from_json(doc["buffers"][i]);
        buffers.emplace_back(move(temp));
    }

    for (auto &buffer : buffers) { buffer.load_data(); }
}

void glTF::ReadBufferViews(const GenericValue<UTF8<>> &doc) {
    bufferViews.clear();
    if (!check_existence(doc, "bufferViews", kArrayType)) { return; }
    for (SizeType i = 0; i < doc["bufferViews"].GetArray().Size(); ++i) {
        bufferView temp(Source, "bufferViews[" + to_string(i) + "]");
        temp.from_json(doc["bufferViews"][i]);
        bufferViews.emplace_back(move(temp));
    }
}

void glTF::ReadCameras(const GenericValue<UTF8<>> &doc) {
    if (!check_existence(doc, "cameras", kArrayType)) { return; }
    cameras.clear();
    for (SizeType i = 0; i < doc["cameras"].GetArray().Size(); ++i) {
        camera temp(Source, "cameras[" + to_string(i) + "]");
        temp.from_json(doc["cameras"][i]);
        cameras.emplace_back(temp);
    }
}

void glTF::ReadImages(const GenericValue<UTF8<>> &doc) {
    if (!check_existence(doc, "images", kArrayType)) { return; }
    images.clear();
    for (SizeType i = 0; i < doc["images"].GetArray().Size(); ++i) {
        image temp(Source, "images[" + to_string(i) + "]");
        temp.from_json(doc["images"][i]);
        images.emplace_back(temp);
    }
}

void glTF::ReadMaterials(const GenericValue<UTF8<>> &doc) {
    if (!check_existence(doc, "materials", kArrayType)) { return; }
    materials.clear();
    for (SizeType i = 0; i < doc["materials"].GetArray().Size(); ++i) {
        material temp(Source, "materials[" + to_string(i) + "]");
        temp.from_json(doc["materials"][i]);
        materials.emplace_back(temp);
    }
    for (auto &material : materials) {
        if (material.pbrMetallicRoughness != nullptr) {
            if (material.pbrMetallicRoughness->baseColorTexture != nullptr) {
                material.pbrMetallicRoughness->baseColorTexture->__set_texture_object(textures);
            }
        }
    }
}

void glTF::ReadMeshes(const GenericValue<UTF8<>> &doc) {
    if (!check_existence(doc, "meshes", kArrayType)) { return; }
    meshes.clear();
    for (SizeType i = 0; i < doc["meshes"].GetArray().Size(); ++i) {
        mesh temp(Source, "meshes[" + to_string(i) + "]");
        temp.from_json(doc["meshes"][i]);
        meshes.emplace_back(temp);
    }
}

void glTF::ReadNodes(const GenericValue<UTF8<>> &doc) {
    if (!check_existence(doc, "nodes", kArrayType)) { return; }
    nodes.clear();
    for (SizeType i = 0; i < doc["nodes"].GetArray().Size(); ++i) {
        node temp(Source, "nodes[" + to_string(i) + "]");
        temp.from_json(doc["nodes"][i]);
        nodes.emplace_back(temp);
    }
}

void glTF::ReadSceneDefault(const GenericValue<UTF8<>> &doc) {
    if (!check_existence(doc, "scene", kNumberType)) { return; }
    scene = doc["scene"].GetUint();
}

void glTF::ReadScenes(const GenericValue<UTF8<>> &doc) {
    if (!check_existence(doc, "scenes", kArrayType)) { return; }
    scenes.clear();
    for (SizeType i = 0; i < doc["scenes"].GetArray().Size(); ++i) {
        scene_t temp(Source, "scenes[" + to_string(i) + "]");
        temp.from_json(doc["scenes"][i]);
        scenes.emplace_back(temp);
    }
}

void glTF::ReadSkins(const GenericValue<UTF8<>> &doc) {
    if (!check_existence(doc, "skins", kArrayType)) { return; }
    skins.clear();
    for (SizeType i = 0; i < doc["skins"].GetArray().Size(); ++i) {
        skin temp(Source, "skins[" + to_string(i) + "]");
        temp.from_json(doc["skins"][i]);
        skins.emplace_back(temp);
    }
}

void glTF::ReadSamplers(const GenericValue<UTF8<>> &doc) {
    if (!check_existence(doc, "samplers", kArrayType)) { return; }
    samplers.clear();
    for (SizeType i = 0; i < doc["samplers"].GetArray().Size(); ++i) {
        animation_sampler temp(Source, "samplers[" + to_string(i) + "]");
        temp.from_json(doc["samplers"][i]);
        samplers.emplace_back(temp);
    }
}

void glTF::ReadTextures(const GenericValue<UTF8<>> &doc) {
    if (!check_existence(doc, "textures", kArrayType)) { return; }

    textures.clear();
    for (SizeType i = 0; i < doc["textures"].GetArray().Size(); ++i) {
        auto temp = make_shared<texture>(Source, "textures[" + to_string(i) + "]");
        temp->from_json(doc["textures"][i]);
        textures.emplace_back(temp);
    }
}

} // namespace gleb
#undef USE_GLEB_UTILS
