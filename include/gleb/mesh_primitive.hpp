/**
 * @file mesh_primitive.hpp
 * @brief This file contains the declaration of the mesh_primitive class.
 */

#ifndef GLEB_MESH_PRIMITIVE_HPP
#define GLEB_MESH_PRIMITIVE_HPP
#include "internal/gltf_property.hpp"

#include <optional>
#include <vector>

namespace gleb {
/**
 * @brief The mesh_primitive class describes the mesh primitive property of a glTF file.
 */
class mesh_primitive : public gltf_property {
public:
    mesh_primitive(const std::string &source, const std::string &id); //!< The default constructor.
    mesh_primitive(const mesh_primitive &other); //!< The copy constructor.

    /**
     * @brief from_json parses the information from the glTF file.
     * @param doc is the JSON to be parsed.
     */
    void from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) override;

    // TODO: The standard is a bit unclear in here.
    // Is there a set of must-have attributes?
    // In the mean time, just copy the whole object and access like a map.
    rapidjson::Document   attributes; // gltf_required
    std::optional<size_t> indices;
    std::optional<size_t> material;

    std::optional<size_t> mode;

    // TODO: write a class for the morph target once it's clear what is exactly inside,
    // and if it's the same every time.
    rapidjson::Document targets;
};
} // namespace gleb
#endif // GLEB_MESH_PRIMITIVE_HPP
