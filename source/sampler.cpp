#define USE_GLEB_UTILS
#include <gleb/sampler.hpp>

#include <gleb/internal/default_values.hpp>
#include <gleb/internal/utils.hpp>
#include <gleb/internal/valid_values.hpp>

namespace gleb {
using namespace rapidjson;

sampler::sampler(const std::string &source, const std::string &id) : gltf_property(source, id) {}

void sampler::from_json(const GenericValue<UTF8<>> &doc) {
    if (check_existence(doc, "magFilter", kNumberType)) {
        magFilter = doc["magFilter"].GetUint();
        if (!check_validity(magFilter.value(), valid_sampler_magFilters)) {
            FileReadError(Source, Identifier, "Invalid magnification filter.");
        }
    }
    if (check_existence(doc, "minFilter", kNumberType)) {
        minFilter = doc["minFilter"].GetUint();
        if (!check_validity(minFilter.value(), valid_sampler_minFilters)) {
            FileReadError(Source, Identifier, "Invalid minification filter.");
        }
    }

    if (check_existence(doc, "wrapS", kNumberType)) {
        wrapS = doc["wrapS"].GetUint();
        if (!check_validity(wrapS.value(), valid_sampler_wraps)) {
            FileReadError(Source, Identifier, "Invalid wrapS value.");
        }
    } else {
        wrapS = default_sampler_wrapS;
    }

    if (check_existence(doc, "wrapT", kNumberType)) {
        wrapT = doc["wrapT"].GetUint();
        if (!check_validity(wrapT.value(), valid_sampler_wraps)) {
            FileReadError(Source, Identifier, "Invalid wrapT value.");
        }
    } else {
        wrapT = default_sampler_wrapT;
    }

    if (check_existence(doc, "name", kStringType)) { name = doc["name"].GetString(); }

    check_and_read_extensions(doc);
    check_and_read_extras(doc);
}
} // namespace gleb
#undef USE_GLEB_UTILS
