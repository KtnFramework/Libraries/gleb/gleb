/**
 * @file animation_channel.hpp
 * @brief This file contains the declaration of the animation_channel class.
 */

#ifndef GLEB_ANIMATION_CHANNEL_HPP
#define GLEB_ANIMATION_CHANNEL_HPP
#include "internal/gltf_property.hpp"

#include "animation_channel_target.hpp"

#include <optional>

namespace gleb {
/**
 * @brief The animation_channel class describes the animation channel property of a glTF file.
 */
class animation_channel : gltf_property {
public:
    animation_channel(const std::string &source, const std::string &id); //!< The default constructor.

    /**
     * @brief from_json parses the information from the glTF file.
     * @param doc is the JSON to be parsed.
     */
    void from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) override;

    size_t                   sampler{}; //!< The index of the associated animation sampler.
    animation_channel_target target; //!< The target of this animation channel.
};
} // namespace gleb
#endif // GLEB_ANIMATION_CHANNEL_HPP
