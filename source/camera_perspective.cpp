#define USE_GLEB_UTILS
#include <gleb/camera_perspective.hpp>

#include <gleb/internal/utils.hpp>
#include <gleb/internal/valid_values.hpp>

namespace gleb {
using namespace rapidjson;

camera_perspective::camera_perspective(const std::string &source, const std::string &id) : gltf_property(source, id) {}

void camera_perspective::from_json(const GenericValue<UTF8<>> &doc) {
    AssertExistence(doc, "yfov", kNumberType);
    AssertExistence(doc, "znear", kNumberType);

    yfov = doc["yfov"].GetFloat();
    if (yfov <= 0) { FileReadError(Source, Identifier, "Invalid yfov value."); }

    znear = doc["znear"].GetFloat();
    if (znear <= 0) { FileReadError(Source, Identifier, "Invalid znear value."); }

    if (check_existence(doc, "aspectRatio", kNumberType)) {
        aspectRatio = doc["aspectRatio"].GetFloat();
        if (aspectRatio.value() <= 0) { FileReadError(Source, Identifier, "Invalid aspect ratio."); }
    }

    if (check_existence(doc, "zfar", kNumberType)) {
        zfar = doc["zfar"].GetFloat();
        if (zfar.value() <= 0) { FileReadError(Source, Identifier, "Invalid zfar value."); }
    }

    check_and_read_extensions(doc);
    check_and_read_extras(doc);
}
} // namespace gleb
#undef USE_GLEB_UTILS
