/**
 * @file node.hpp
 * @brief This file contains the declaration of the node class.
 */

#ifndef GLEB_NODE_HPP
#define GLEB_NODE_HPP
#include "internal/gltf_property.hpp"

#include "internal/valid_values.hpp"

#include <optional>
#include <vector>

namespace gleb {
/**
 * @brief The node class describes the node property of a glTF file.
 */
class node : public gltf_property {
public:
    node(const std::string &source, const std::string &id); //!< The default constructor.

    /**
     * @brief from_json parses the information from the glTF file.
     * @param doc is the JSON to be parsed.
     */
    void from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) override;

    std::optional<size_t> camera; //!< The index of the optionally associated camera.
    std::vector<size_t>   children; //!< The indices of the children nodes.
    std::optional<size_t> skin; //!< The index of the optionally associated skin.
    std::optional<std::array<float, number_of_elements_matrix4x4>> matrix; //!< The optional transformation matrix.
    std::optional<size_t>               mesh; //!< The index of the optionally associated mesh.
    std::optional<std::array<float, 4>> rotation; //!< The optional rotation quaternion. Ordering: x, y, z, w.
    std::optional<std::array<float, 3>> scale; //!< The optional scale vector.
    std::optional<std::array<float, 3>> translation; //!< The optional translation vector.
    std::vector<float>                  weights; //!< The weights of the morph targets when indicated by \ref node.mesh.
    std::optional<std::string>          name; //!< The optional name of the node.
};
} // namespace gleb
#endif // GLEB_NODE_HPP
