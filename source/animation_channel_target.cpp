#define USE_GLEB_UTILS
#include <gleb/animation_channel_target.hpp>

#include <gleb/internal/default_values.hpp>
#include <gleb/internal/utils.hpp>
#include <gleb/internal/valid_values.hpp>

#include <optional>

namespace gleb {
using namespace rapidjson;

animation_channel_target::animation_channel_target(const std::string &source, const std::string &id)
    : gltf_property(source, id) {}

void animation_channel_target::from_json(const GenericValue<UTF8<>> &doc) {
    AssertExistence(doc, "path", kStringType);
    path = doc["path"].GetString();
    if (!check_validity(path, valid_animation_target_paths)) {
        FileReadError(Source, Identifier, "Invalid animation channel target path.");
    }
    if (check_existence(doc, "node", kNumberType)) { node = doc["node"].GetUint(); }
    check_and_read_extensions(doc);
    check_and_read_extras(doc);
}
} // namespace gleb
