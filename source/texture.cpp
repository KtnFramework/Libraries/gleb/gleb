#define USE_GLEB_UTILS
#include <gleb/texture.hpp>

#include <gleb/internal/utils.hpp>

namespace gleb {
using namespace rapidjson;

texture::texture(const std::string &source, const std::string &id) : gltf_property(source, id) {}

void texture::from_json(const GenericValue<UTF8<>> &doc) {
    if (check_existence(doc, "sampler", kNumberType)) { sampler = doc["sampler"].GetUint(); }
    if (check_existence(doc, "source", kNumberType)) { source = doc["source"].GetUint(); }
    if (check_existence(doc, "name", kStringType)) { name = doc["name"].GetString(); }
    check_and_read_extensions(doc);
    check_and_read_extras(doc);
}
} // namespace gleb
#undef USE_GLEB_UTILS
