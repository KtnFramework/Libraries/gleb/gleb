/**
 * @file textureInfo.hpp
 * @brief This file contains the declaration of the textureInfo class.
 */

#ifndef GLEB_TEXTUREINFO_HPP
#define GLEB_TEXTUREINFO_HPP
#include "internal/gltf_property.hpp"

#include "texture.hpp"

#include <vector>

namespace gleb {
/**
 * @brief The textureInfo class describes the textureInfo property of a glTF file.
 */
class textureInfo : gltf_property {
public:
    textureInfo(const std::string &source, const std::string &id); //!< The default constructor.

    /**
     * @brief from_json parses the information from the glTF file.
     * @param doc is the JSON to be parsed.
     */
    void from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) override;

    size_t index{}; // gltf_required

    size_t texCoord = 0;

    void                     __set_texture_object(std::vector<std::shared_ptr<texture>> &);
    std::shared_ptr<texture> textureObject() const; //!< The texture refered to by the index.
private:
    std::shared_ptr<texture> m_texture = nullptr;
};
} // namespace gleb
#endif // GLEB_TEXTUREINFO_HPP
