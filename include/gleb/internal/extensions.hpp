/**
 * @file extensions.hpp
 * @brief This file contains the declaration of the extensions_t class.
 */

#ifndef GLEB_INTERNAL_EXTENSIONS_HPP
#define GLEB_INTERNAL_EXTENSIONS_HPP
#include "property_base.hpp"

namespace gleb {
/**
 * @brief The extensions_t class describes the extensions property of a glTF file.
 * Due to them being non-standard, they are to be parsed on a case-by-case basis in application code.
 */
class extensions_t : property_base_t {
public:
    extensions_t(const std::string &source, const std::string &id); //!< The default constructor.
    ~extensions_t() override; //!< The destructor.

    /**
     * @brief from_json copy the extension object into \ref extensions_t.Data.
     * @param doc is the JSON to be copied.
     */
    void from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) override;

    rapidjson::Document Data;
};
} // namespace gleb
#endif // GLEB_INTERNAL_EXTENSIONS_HPP
