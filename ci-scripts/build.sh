#!/bin/sh
# parse arguments
GLEB_EXAMPLES_TOGGLE=OFF
GLEB_FORCE_REBUILD=FALSE
for i in "$@"; do
    case $i in
    --build-examples)
        GLEB_EXAMPLES_TOGGLE=ON
        ;;
    -f|--force-rebuild)
        GLEB_FORCE_REBUILD=TRUE
    esac
done



if [ -d build ]; then
    if [ "$GLEB_FORCE_REBUILD" = TRUE ]; then
        rm -rf build/
        mkdir build
    fi
else
    mkdir build
fi

cd build

cmake -DGLEB_BUILD_EXAMPLES=$GLEB_EXAMPLES_TOGGLE .. || { exit 1; }
make -j$(nproc) || { exit 1; }
