/**
 * @file asset.hpp
 * @brief This file contains the declaration of the asset_t class.
 */

#ifndef GLEB_ASSET_HPP
#define GLEB_ASSET_HPP
#include "internal/gltf_property.hpp"

#include <optional>

namespace gleb {
/**
 * @brief The asset_t class describes the asset property of a glTF file.
 */
class asset_t : gltf_property {
public:
    asset_t(const std::string &source, const std::string &id); //!< The default constructor.

    /**
     * @brief from_json parses the information from the glTF file.
     * @param doc is the JSON to be parsed.
     */
    void from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) override;

    std::optional<std::string> copyright; //!< The optional copyright information.
    std::optional<std::string> generator; //!< The optional information regarding this asset's generator.
    std::string                version; //!< The targeted glTF version.
    std::optional<std::string> minVersion; //!< The minimum targeted glTF version.
};
} // namespace gleb
#endif // GLEB_ASSET_HPP
