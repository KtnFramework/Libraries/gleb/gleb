#include <gleb/internal/extensions.hpp>

namespace gleb {
using namespace rapidjson;

extensions_t::extensions_t(const std::string &source, const std::string &id) : property_base_t(source, id) {}

extensions_t::~extensions_t() = default;

void extensions_t::from_json(const GenericValue<UTF8<>> &doc) {
    Data.CopyFrom(doc, Data.GetAllocator());
}
} // namespace gleb
