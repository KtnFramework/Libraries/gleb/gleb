/**
 * @file utils.hpp
 * @brief This file contains the utility functions used in various gleb classes.
 */

#ifndef GLEB_INTERNAL_UTILS_HPP
#define GLEB_INTERNAL_UTILS_HPP
#include <rapidjson/document.h>

#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

namespace gleb {
#ifdef USE_GLEB_UTILS
// TODO: replace this macro with std::source_location::filename, line, function_name
// once they are available
#define ReportAndCrash(message) \
    do { \
        std::cout << "In file " __FILE__ << ", line " << __LINE__ << ":\n"; \
        std::cout << message << std::endl; \
        throw(std::runtime_error(message)); \
    } while (0)

#define FileReadError(file, id, message) \
    do { \
        ReportAndCrash("Error while loading " + file + ":\n" + "Object identifier: " + id + ":\n" + message); \
    } while (0)

#define AssertExistence(doc, name, type) \
    do { \
        if (!gleb::check_existence(doc, name, type)) { \
            ReportAndCrash( \
                "Error while loading \"" + Source + "\": " + Identifier + "." + name + \
                " is absent or has the wrong type."); \
        } \
    } while (0)

#define AssertArrayOfNumbers(doc, name) \
    do { \
        for (auto &element : doc[name].GetArray()) { \
            if (!element.IsNumber()) { \
                ReportAndCrash( \
                    "Error while loading \"" + Source + "\": in \"" + Identifier + "\":\n" + name + \
                    " is not an array of numbers."); \
            } \
        } \
    } while (0)

inline std::string AbsenceReport(const std::string &path, const std::string &absentee) {
    return std::string("In file \"" + path + "\": " + absentee + " is absent or has the wrong type.");
}

inline bool check_existence(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc, const std::string &name) {
    return doc.HasMember(name.c_str());
}

/**
 * @brief check_existence checks if a member with the specified name and type
 * exists within the given JSON document.
 * @param document is the examined document.
 * @param eName is the name of the member whose existence is to be determined.
 * @param eType is the type of the member whose existence is to be determined.
 * @return true if such a member with the same name and type is found, false otherwise.
 */
inline bool
check_existence(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc, const std::string &name, rapidjson::Type type) {
    if (!doc.HasMember(name.c_str())) { return false; }

    switch (type) {
    case rapidjson::Type::kNullType:
        if (doc[name.c_str()].IsNull()) { return true; }
        break;
    case rapidjson::Type::kFalseType:
        if (doc[name.c_str()].IsFalse()) { return true; }
        break;
    case rapidjson::Type::kTrueType:
        if (doc[name.c_str()].IsTrue()) { return true; }
        break;
    case rapidjson::Type::kObjectType:
        if (doc[name.c_str()].IsObject()) { return true; }
        break;
    case rapidjson::Type::kArrayType:
        if (doc[name.c_str()].IsArray()) { return true; }
        break;
    case rapidjson::Type::kStringType:
        if (doc[name.c_str()].IsString()) { return true; }
        break;
    case rapidjson::Type::kNumberType:
        if (doc[name.c_str()].IsNumber()) { return true; }
        break;
    }

    return false;
}

inline bool check_existence_array(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc, const std::string &name) {
    return check_existence(doc, name, rapidjson::Type::kArrayType);
}

inline bool check_existence_bool(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc, const std::string &name) {
    const bool isFalseType = check_existence(doc, name, rapidjson::Type::kFalseType);
    const bool isTrueType  = check_existence(doc, name, rapidjson::Type::kTrueType);
    return isFalseType || isTrueType;
}

template <typename T> bool check_validity(T value, const std::vector<T> &validValues) {
    return (std::find(validValues.begin(), validValues.end(), value) != validValues.end());
}

template <typename T, size_t S> bool check_validity(T value, const std::array<T, S> &validValues) {
    return (std::find(validValues.begin(), validValues.end(), value) != validValues.end());
}

inline std::string LocationReport(const std::string &file, size_t line, const std::string &message) {
    return std::string("In file ") + file + ", line " + std::to_string(line) + ": " + message;
}

#endif // USE_GLEB_UTILS
} // namespace gleb
#endif // GLEB_INTERNAL_UTILS_HPP
