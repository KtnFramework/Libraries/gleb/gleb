/**
 * @file texture.hpp
 * @brief This file contains the declaration of the texture class.
 */

#ifndef GLEB_TEXTURE_HPP
#define GLEB_TEXTURE_HPP
#include "internal/gltf_property.hpp"

#include <optional>
#include <string>

namespace gleb {
/**
 * @brief The texture class describes the texture property of a glTF file.
 */
class texture : gltf_property {
public:
    texture(const std::string &source, const std::string &id); //!< The default constructor.

    /**
     * @brief from_json parses the information from the glTF file.
     * @param doc is the JSON to be parsed.
     */
    void from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) override;

    std::optional<size_t>      sampler; //!< The index of the optional sampler.
    std::optional<size_t>      source; //!< The index of the optional source image.
    std::optional<std::string> name; //!< The optional name of the texture.
};
} // namespace gleb
#endif // GLEB_TEXTURE_HPP
