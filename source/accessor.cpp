#define USE_GLEB_UTILS
#include <gleb/accessor.hpp>

#include <gleb/internal/utils.hpp>
#include <gleb/internal/valid_values.hpp>

#include <algorithm>

namespace gleb {
using namespace rapidjson;

accessor::accessor(const std::string &source, const std::string &id) : gltf_property(source, id) {}

void accessor::from_json(const GenericValue<UTF8<>> &doc) {
    AssertExistence(doc, "componentType", kNumberType);
    AssertExistence(doc, "count", kNumberType);
    AssertExistence(doc, "type", kStringType);
    bool bufferViewExists = check_existence(doc, "bufferView", kNumberType);
    bool byteOffsetExists = check_existence(doc, "byteOffset", kNumberType);
    bool normalizedExists = check_existence_bool(doc, "normalized");
    bool maxExists        = check_existence_array(doc, "max");
    bool minExists        = check_existence_array(doc, "min");
    bool sparseExists     = check_existence(doc, "sparse", kObjectType);
    bool nameExists       = check_existence(doc, "name", kStringType);

    componentType = doc["componentType"].GetUint();
    if (valid_accessor_component_types.end() ==
        std::find(valid_accessor_component_types.begin(), valid_accessor_component_types.end(), componentType)) {
        ReportAndCrash("Component type is not valid.");
    }

    count = doc["count"].GetUint();
    if (count == 0) { ReportAndCrash("Count must be >= 1."); }

    type = doc["type"].GetString();
    if (find(valid_accessor_types.begin(), valid_accessor_types.end(), type) == valid_accessor_types.end()) {
        ReportAndCrash("Accessor type is not valid.");
    }

    if (bufferViewExists) { bufferView = doc["bufferView"].GetUint(); }

    if (byteOffsetExists) { byteOffset = doc["byteOffset"].GetUint(); }

    if (normalizedExists) { normalized = doc["normalized"].GetBool(); }

    if (maxExists) {
        AssertArrayOfNumbers(doc, "max");
        max = std::vector<float>();
        for (auto &temp : doc["max"].GetArray()) { max.push_back(temp.GetFloat()); }
        if (valid_accessor_max_min_lengths.end() ==
            std::find(valid_accessor_max_min_lengths.begin(), valid_accessor_max_min_lengths.end(), max.size())) {
            ReportAndCrash("Assessor max array has an invalid number of elements.");
        }
    }

    if (minExists) {
        AssertArrayOfNumbers(doc, "min");
        min = std::vector<float>();
        for (auto &temp : doc["min"].GetArray()) { min.push_back(temp.GetFloat()); }
        if (valid_accessor_max_min_lengths.end() ==
            std::find(valid_accessor_max_min_lengths.begin(), valid_accessor_max_min_lengths.end(), min.size())) {
            ReportAndCrash("Assessor min array has an invalid number of elements.");
        }
    }

    if (sparseExists) { sparse->from_json(doc["sparse"]); }

    if (nameExists) { name = doc["name"].GetString(); }

    check_and_read_extensions(doc);
    check_and_read_extras(doc);
}
} // namespace gleb
#undef USE_GLEB_UTILS
