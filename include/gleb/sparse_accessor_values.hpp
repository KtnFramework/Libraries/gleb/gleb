/**
 * @file sparse_accessor_values.hpp
 * @brief This file contains the declaration of the sparse_accessor_values class.
 */

#ifndef GLEB_SPARSE_ACCESSOR_VALUES_HPP
#define GLEB_SPARSE_ACCESSOR_VALUES_HPP
#include "internal/gltf_property.hpp"

#include <optional>

namespace gleb {
/**
 * @brief The sparse_accessor_values class describes the sparse accessor values property of a glTF file.
 */
class sparse_accessor_values : gltf_property {
public:
    sparse_accessor_values(const std::string &source, const std::string &id); //!< The default constructor.

    /**
     * @brief from_json parses the information from the glTF file.
     * @param doc is the JSON to be parsed.
     */
    void from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) override;

    size_t                bufferView{}; //!< The index of the associated bufferView;
    std::optional<size_t> byteOffset; //!< The optional byte offset.
};
} // namespace gleb
#endif // GLEB_SPARSE_ACCESSOR_VALUES_HPP
