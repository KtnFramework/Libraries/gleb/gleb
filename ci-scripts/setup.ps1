param(
    [switch]$force = $false,

    [bool]$gtest = $true,
    [bool]$rapidjson = $true
)

function download_or_update {
    param(
        $destination,
        $source
    )
    if (Test-Path $destination) {
        Write-Output "Updating $destination..."
        Set-Location $destination
        git pull
        if (!$?) {
            Write-Output "Error updating $destination! Downloading instead..."
        
            Set-Location ..
            Remove-Item -Recurse -Force $destination
            download_or_update $destination $source
        }
        else {
            Set-Location ..
        }
    }
    else {
        Write-Output "Downloading $destination..."
        git clone $source
        if (!$?) {
            Write-Output "Error downloading $destination!"
            exit 1
        }
        Write-Output "Dependency `"$destination`" from $source downloaded."
    }
}

MSBuild -version; if (!$?) {
    Write-Output "The directory for MSBuild.exe from the Visual Studio Build Tools is not in PATH."; exit 1
}

7z *>$null; if (!$?) {
    Write-Output "The directory for 7z.exe is not in PATH."; exit 1
}

if ($force) {
    Write-Output "Removing existing external libraries folder..."
    Remove-Item external -Recurse -Force
}

# create external directory if it's not already there
if ([IO.Directory]::Exists((Join-Path(Get-Location) 'external'))) {
    Write-Output "Working with existing external libraries folder. To force setup with new folder, use the -force switch.";
}
else {
    mkdir external
}

Set-Location external
Write-Output "Downloading external libraries..."

if ($gtest) {
    $googletest_archive = "googletest.zip"
    $googletest_download_target = "$PWD/$googletest_archive"
    $googletest_url = "https://github.com/google/googletest/archive/master.zip"
        
    if (Test-Path googletest) {
        Remove-Item googletest -Recurse -Force
    }
    if (Test-Path $googletest_archive) {
        Remove-Item $googletest_archive
    }
    Write-Output "Downloading googletest..."
    (New-Object System.Net.WebClient).DownloadFile($googletest_url, $googletest_download_target); if (!$?) {
        Write-Output "Could not download googletest."; exit 1
    }
    7z x $googletest_download_target; if (!$?) {
        Write-Output "Could not extract $googletest_archive."; exit 1
    }
    Remove-Item $googletest_archive
    Rename-Item googletest-master googletest
}

# To reduce the amount of warnings caused by cpp17 deprecation messages, use master branch instead of v1.1.0 release
if ($rapidjson) { download_or_update -destination rapidjson -source https://github.com/Tencent/rapidjson.git }

# TODO: use this again when rapidjson releases newer version
# if ($rapidjson) {
#     $rapidjson_version = "1.1.0"
#     $rapidjson_archive = "rapidjson-$rapidjson_version.zip"
#     $rapidjson_download_target = "$PWD/$rapidjson_archive"
#     $rapidjson_url = "https://codeload.github.com/Tencent/rapidjson/zip/v$rapidjson_version"
        
#     if (Test-Path rapidjson) {
#         Remove-Item rapidjson -Recurse -Force
#     }
#     if (Test-Path $rapidjson_archive) {
#         Remove-Item $rapidjson_archive
#     }
#     Write-Output "Downloading rapidjson..."
#     (New-Object System.Net.WebClient).DownloadFile($rapidjson_url, $rapidjson_download_target); if (!$?) {
#         Write-Output "Could not download rapidjson."; Set-Location ..; exit 1
#     }
#     7z x $rapidjson_download_target; if (!$?) {
#         Write-Output "Could not extract $rapidjson_archive."; Set-Location ..; exit 1
#     }
#     Remove-Item $rapidjson_archive
#     Rename-Item rapidjson-$rapidjson_version rapidjson
# }

Set-Location ..