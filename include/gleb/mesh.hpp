/**
 * @file mesh.hpp
 * @brief This file contains the declaration of the mesh class.
 */

#ifndef GLEB_MESH_HPP
#define GLEB_MESH_HPP
#include "internal/gltf_property.hpp"

#include "mesh_primitive.hpp"

#include <optional>
#include <vector>

namespace gleb {
/**
 * @brief The mesh class describes the mesh property of a glTF file.
 */
class mesh : public gltf_property {
public:
    mesh(const std::string &source, const std::string &id); //!< The default constructor.

    /**
     * @brief from_json parses the information from the glTF file.
     * @param doc is the JSON to be parsed.
     */
    void from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) override;

    std::vector<mesh_primitive> primitives; //!< The array of associated primitives.
    std::vector<float>          weights; //!< The optional array of morph target weights.
    std::optional<std::string>  name; //!< The optional name of the mesh.
};
} // namespace gleb
#endif // GLEB_MESH_HPP
