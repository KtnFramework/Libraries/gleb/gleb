/**
 * @file sparse_accessor.hpp
 * @brief This file contains the declaration of the sparse_accessor class.
 */

#ifndef GLEB_SPARSE_ACCESSOR_HPP
#define GLEB_SPARSE_ACCESSOR_HPP
#include "internal/gltf_property.hpp"

#include "sparse_accessor_indices.hpp"
#include "sparse_accessor_values.hpp"

namespace gleb {
/**
 * @brief The sparse_accessor class describes the sparse accessor property of a glTF file.
 */
class sparse_accessor : gltf_property {
public:
    sparse_accessor(const std::string &source, const std::string &id); //!< The default constructor.

    /**
     * @brief from_json parses the information from the glTF file.
     * @param doc is the JSON to be parsed.
     */
    void from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) override;

    size_t                  count{}; //!< The number of displaced attributes.
    sparse_accessor_indices indices; //!< The indices of displaced attributes.
    sparse_accessor_values  values; //!< The values of displaced attributes.
};
} // namespace gleb
#endif // GLEB_SPARSE_ACCESSOR_HPP
