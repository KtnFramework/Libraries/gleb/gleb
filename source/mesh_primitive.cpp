#define USE_GLEB_UTILS
#include <gleb/mesh_primitive.hpp>

#include <gleb/internal/default_values.hpp>
#include <gleb/internal/utils.hpp>
#include <gleb/internal/valid_values.hpp>

namespace gleb {
using namespace rapidjson;
using namespace std;

mesh_primitive::mesh_primitive(const string &source, const string &id) : gltf_property(source, id) {}

mesh_primitive::mesh_primitive(const mesh_primitive &other) : gltf_property(other) {
    attributes.CopyFrom(other.attributes, attributes.GetAllocator());
    indices  = other.indices;
    material = other.material;
    mode     = other.mode;
    targets.CopyFrom(other.targets, targets.GetAllocator());
}

void mesh_primitive::from_json(const GenericValue<UTF8<>> &doc) {
    AssertExistence(doc, "attributes", kObjectType);
    attributes.CopyFrom(doc["attributes"], attributes.GetAllocator());

    if (check_existence(doc, "indices", kNumberType)) { indices = doc["indices"].GetUint(); }

    if (check_existence(doc, "material", kNumberType)) { material = doc["material"].GetUint(); }

    if (check_existence(doc, "mode", kNumberType)) {
        mode = doc["mode"].GetUint();
        if (!check_validity(mode.value(), valid_mesh_primitive_modes)) {
            FileReadError(Source, Identifier, "Invalid mesh primitive mode.");
        }
    } else {
        mode = default_mesh_primitive_mode;
    }

    if (check_existence(doc, "targets", kArrayType)) { targets.CopyFrom(doc["targets"], targets.GetAllocator()); }

    check_and_read_extensions(doc);
    check_and_read_extras(doc);
}
} // namespace gleb
