/**
 * @file camera.hpp
 * @brief This file contains the declaration of the camera class.
 */

#ifndef GLEB_CAMERA_HPP
#define GLEB_CAMERA_HPP
#include "internal/gltf_property.hpp"

#include "camera_orthographic.hpp"
#include "camera_perspective.hpp"
#include <optional>
#include <vector>

namespace gleb {
/**
 * @brief The camera class describes the camera property of a glTF file.
 */
class camera : gltf_property {
public:
    camera(const std::string &source, const std::string &id); //!< The default constructor.

    /**
     * @brief from_json parses the information from the glTF file.
     * @param doc is the JSON to be parsed.
     */
    void from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) override;

    std::optional<camera_orthographic> orthographic;
    std::optional<camera_perspective>  perspective;
    std::string                        type; // gltf_required
    std::optional<std::string>         name;
};
} // namespace gleb
#endif // GLEB_CAMERA_HPP
