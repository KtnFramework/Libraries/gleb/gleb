# GLEB
GLEB is a glTF loader/saver written in C++ 17.

## Prerequisites
- CMake
- A C++ 17 compiler and libraries (gcc9, MSVC19, etc.)

## Dependencies
- [rapidjson](https://github.com/Tencent/rapidjson)

# FAQ
## Which operating systems are supported?
- The CI scripts have been tested on:
  - Ubuntu 19.10
- Ubuntu 19.04 and before are not supported due to incompelete implementation
  of std::filesystem, especially 19.04, which is able to build GLEB, but links
  the wrong library and crashes the examples.
