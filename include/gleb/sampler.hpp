/**
 * @file sampler.hpp
 * @brief This file contains the declaration of the sampler class.
 */

#ifndef GLEB_SAMPLER_HPP
#define GLEB_SAMPLER_HPP
#include "internal/gltf_property.hpp"

#include <optional>
#include <string>
#include <vector>

namespace gleb {
/**
 * @brief The sampler class describes the sampler property of a glTF file.
 */
class sampler : gltf_property {
public:
    sampler(const std::string &source, const std::string &id); //!< The default constructor.

    /**
     * @brief from_json parses the information from the glTF file.
     * @param doc is the JSON to be parsed.
     */
    void from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) override;

    std::optional<size_t>      magFilter; //!< The optional magnification filter.
    std::optional<size_t>      minFilter; //!< The optional minification filter.
    std::optional<size_t>      wrapS; //!< The optional S wrapping mode.
    std::optional<size_t>      wrapT; //!< The optional T wrapping mode.
    std::optional<std::string> name; //!< The optional name of the sampler.
};
} // namespace gleb
#endif // GLEB_SAMPLER_HPP
