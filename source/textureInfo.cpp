#define USE_GLEB_UTILS
#include <gleb/textureInfo.hpp>

#include <gleb/internal/utils.hpp>

namespace gleb {
using namespace rapidjson;
using namespace std;

textureInfo::textureInfo(const std::string &source, const std::string &id) : gltf_property(source, id) {}

void textureInfo::from_json(const GenericValue<UTF8<>> &doc) {
    AssertExistence(doc, "index", kNumberType);
    index = doc["index"].GetUint();

    if (check_existence(doc, "texCoord", kNumberType)) { texCoord = doc["texCoord"].GetUint(); }
    check_and_read_extensions(doc);
    check_and_read_extras(doc);
}

void textureInfo::__set_texture_object(vector<shared_ptr<texture>> &textures) {
    m_texture = textures.at(index);
}

shared_ptr<texture> textureInfo::textureObject() const {
    return m_texture;
}
} // namespace gleb
#undef USE_GLEB_UTILS
