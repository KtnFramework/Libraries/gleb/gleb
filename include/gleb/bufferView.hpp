/**
 * @file bufferView.hpp
 * @brief This file contains the declaration of the bufferView class.
 */

#ifndef GLEB_BUFFERVIEW_HPP
#define GLEB_BUFFERVIEW_HPP
#include "internal/gltf_property.hpp"

#include <optional>

namespace gleb {
/**
 * @brief The bufferView class describes the bufferView property of a glTF file.
 */
class bufferView : gltf_property {
public:
    bufferView(const std::string &source, const std::string &id); //!< The default constructor.

    /**
     * @brief from_json parses the information from the glTF file.
     * @param doc is the JSON to be parsed.
     */
    void from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) override;

    size_t                     buffer{}; // gltf_required
    std::optional<size_t>      byteOffset;
    size_t                     byteLength{}; // gltf_required
    std::optional<size_t>      byteStride;
    std::optional<size_t>      target;
    std::optional<std::string> name;
};
} // namespace gleb
#endif // GLEB_BUFFERVIEW_HPP
