/**
 * @file skin.hpp
 * @brief This file contains the declaration of the skin class.
 */

#ifndef GLEB_SKIN_HPP
#define GLEB_SKIN_HPP
#include "internal/gltf_property.hpp"

#include <optional>
#include <vector>

namespace gleb {
/**
 * @brief The skin class describes the skin property of a glTF file.
 */
class skin : gltf_property {
public:
    skin(const std::string &source, const std::string &id); //!< The default constructor.

    /**
     * @brief from_json parses the information from the glTF file.
     * @param doc is the JSON to be parsed.
     */
    void from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) override;

    std::optional<size_t>      inverseBindMatrices; //!< The index of the bind matrices' accessor.
    std::optional<size_t>      skeleton; //!< The index of the skeleton root's node.
    std::vector<size_t>        joints; //!< Indices of the joints' accessors.
    std::optional<std::string> name; //!< The optional name of the skin.
};
} // namespace gleb
#endif // GLEB_SKIN_HPP
