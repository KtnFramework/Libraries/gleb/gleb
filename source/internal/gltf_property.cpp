#define USE_GLEB_UTILS
#include <gleb/internal/gltf_property.hpp>
#include <gleb/internal/utils.hpp>

namespace gleb {
using namespace rapidjson;

gltf_property::gltf_property(const std::string &source, const std::string &id) : property_base_t(source, id) {}

gltf_property::~gltf_property() = default;

void gltf_property::check_and_read_extensions(const GenericValue<UTF8<>> &doc) {
    if (check_existence(doc, "extensions", kObjectType)) {
        extensions = std::make_shared<extensions_t>(Source, Identifier + ".extensions");
        extensions->from_json(doc["extensions"]);
    }
}

void gltf_property::check_and_read_extras(const GenericValue<UTF8<>> &doc) {
    if (check_existence(doc, "extras")) {
        extras = std::make_shared<extras_t>(Source, Identifier + ".extras");
        extras->from_json(doc["extras"]);
    }
}
} // namespace gleb
#undef USE_GLEB_UTILS
