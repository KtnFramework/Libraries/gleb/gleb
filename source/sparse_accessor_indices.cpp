#define USE_GLEB_UTILS
#include <gleb/sparse_accessor_indices.hpp>

#include <gleb/internal/default_values.hpp>
#include <gleb/internal/utils.hpp>
#include <gleb/internal/valid_values.hpp>

namespace gleb {
using namespace rapidjson;

sparse_accessor_indices::sparse_accessor_indices(const std::string &source, const std::string &id)
    : gltf_property(source, id) {}

void sparse_accessor_indices::from_json(const GenericValue<UTF8<>> &doc) {
    AssertExistence(doc, "bufferView", kNumberType);
    AssertExistence(doc, "componentType", kObjectType);

    bool byteOffsetExists = check_existence(doc, "byteOffset", kNumberType);

    bufferView    = doc["bufferView"].GetUint();
    componentType = static_cast<size_t>(doc["componentType"].GetUint());
    if (valid_sparse_accessor_indices_component_types.end() ==
        std::find(
            valid_sparse_accessor_indices_component_types.begin(),
            valid_sparse_accessor_indices_component_types.end(),
            componentType)) {
        FileReadError(Source, Identifier, "Invalid component type.");
    }
    if (byteOffsetExists) {
        byteOffset = doc["byteOffset"].GetUint();
    } else {
        byteOffset = default_byteOffset;
    }
    check_and_read_extensions(doc);
    check_and_read_extras(doc);
}
} // namespace gleb
