#define USE_GLEB_UTILS
#include <gleb/skin.hpp>

#include <gleb/internal/utils.hpp>

namespace gleb {
using namespace rapidjson;

skin::skin(const std::string &source, const std::string &id) : gltf_property(source, id) {}

void skin::from_json(const GenericValue<UTF8<>> &doc) {
    AssertExistence(doc, "joints", kArrayType);

    joints.clear();
    for (SizeType i = 0; i < doc["joints"].GetArray().Size(); ++i) { joints.emplace_back(doc["joints"][i].GetUint()); }

    if (check_existence(doc, "inverseBindMatrices", kNumberType)) {
        inverseBindMatrices = doc["inverseBindMatrices"].GetUint();
    }

    if (check_existence(doc, "skeleton", kNumberType)) { skeleton = doc["skeleton"].GetUint(); }

    if (check_existence(doc, "name", kStringType)) { name = doc["name"].GetString(); }

    check_and_read_extensions(doc);
    check_and_read_extras(doc);
}
} // namespace gleb
#undef USE_GLEB_UTILS
