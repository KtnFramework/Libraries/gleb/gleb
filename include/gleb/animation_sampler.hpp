/**
 * @file animation_sampler.hpp
 * @brief This file contains the declaration of the animation_sampler class.
 */

#ifndef GLEB_ANIMATION_SAMPLER_HPP
#define GLEB_ANIMATION_SAMPLER_HPP
#include "internal/gltf_property.hpp"

#include <optional>

namespace gleb {
/**
 * @brief The animation_sampler class describes the animation sampler property of a glTF file.
 */
class animation_sampler : gltf_property {
public:
    animation_sampler(const std::string &source, const std::string &id); //!< The default constructor.

    /**
     * @brief from_json parses the information from the glTF file.
     * @param doc is the JSON to be parsed.
     */
    void from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) override;

    size_t                     input{}; //!< The index of the sampling time's accessor.
    std::optional<std::string> interpolation; //!< The optional interpolation algorithm.
    size_t                     output{}; //!< The index of the output transformation's accessor.
};
} // namespace gleb
#endif // GLEB_ANIMATION_SAMPLER_HPP
