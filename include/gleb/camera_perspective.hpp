/**
 * @file camera_perspective.hpp
 * @brief This file contains the declaration of the camera_perspective class.
 */

#ifndef GLEB_CAMERA_PERSPECTIVE_HPP
#define GLEB_CAMERA_PERSPECTIVE_HPP
#include "internal/gltf_property.hpp"

#include <optional>

namespace gleb {
/**
 * @brief The camera_perspective class describes the perspective camera property of a glTF file.
 */
class camera_perspective : gltf_property {
public:
    camera_perspective(const std::string &source, const std::string &id); //!< The default constructor.

    /**
     * @brief from_json parses the information from the glTF file.
     * @param doc is the JSON to be parsed.
     */
    void from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) override;

    std::optional<float> aspectRatio;
    float                yfov{}; // gltf_required
    std::optional<float> zfar;
    float                znear{}; // gltf_required
};
} // namespace gleb
#endif // GLEB_CAMERA_PERSPECTIVE_HPP
