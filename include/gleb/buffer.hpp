/**
 * @file buffer.hpp
 * @brief This file contains the declaration of the buffer class.
 */

#ifndef GLEB_BUFFER_HPP
#define GLEB_BUFFER_HPP
#include "internal/gltf_property.hpp"

#include <optional>
#include <vector>

namespace gleb {
/**
 * @brief The buffer class describes the buffer property of a glTF file.
 */
class buffer : gltf_property {
public:
    buffer(const std::string &source, const std::string &id); //!< The default constructor.

    /**
     * @brief from_json parses the information from the glTF file.
     * @param doc is the JSON to be parsed.
     */
    void from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) override;

    [[nodiscard]] std::vector<char> data(const size_t &starting_index = 0, const size_t &length = SIZE_MAX) const;
    bool                            data_loaded();
    void                            load_data();
    void                            unload_data();

    std::optional<std::string> uri;
    size_t                     byteLength{}; // gltf_required
    std::optional<std::string> name;

private:
    std::vector<char> m_Data;
    bool              m_IsLoaded = false;
};
} // namespace gleb
#endif // GLEB_BUFFER_HPP
