/**
 * @file gltf_property.hpp
 * @brief This file contains the declaration of the gltf_property class.
 */

#ifndef GLEB_INTERNAL_GLTF_PROPERTY_HPP
#define GLEB_INTERNAL_GLTF_PROPERTY_HPP
#include "property_base.hpp"

#include "extensions.hpp"
#include "extras.hpp"

#include <rapidjson/document.h>

#include <memory>

namespace gleb {
/**
 * @brief The gltf_property class describes the base class for all glTF properties,
 * except \ref extensions_t and \ref extras_t, which derive from \ref property_base_t.
 */
class gltf_property : public property_base_t {
public:
    gltf_property(const std::string &source, const std::string &id); //!< The default constructor.
    ~gltf_property() override; //!< The destructor.

    /**
     * @brief from_json parses the information from the glTF file.
     * @param doc is the JSON to be parsed.
     */
    void from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) override = 0;

    void check_and_read_extensions(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc);
    void check_and_read_extras(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc);

    std::shared_ptr<extensions_t> extensions;
    std::shared_ptr<extras_t>     extras;
};
} // namespace gleb
#endif // GLEB_INTERNAL_GLTF_PROPERTY_HPP
