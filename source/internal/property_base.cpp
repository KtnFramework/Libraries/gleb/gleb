#include <gleb/internal/property_base.hpp>

namespace gleb {
property_base_t::property_base_t(const std::string &source, const std::string &id) : Identifier(id), Source(source) {}

property_base_t::~property_base_t() = default;
} // namespace gleb
