/**
 * @file material.hpp
 * @brief This file contains the declaration of the material class.
 */

#ifndef GLEB_MATERIAL_HPP
#define GLEB_MATERIAL_HPP
#include "internal/gltf_property.hpp"
#include "textureInfo.hpp"

#include <array>
#include <optional>
#include <utility>
#include <vector>

namespace gleb {
/**
 * @brief The material class describes the material property of a glTF file.
 */
class material : gltf_property {
public:
    // TODO: detach these too
    /**
     * @brief The normalTextureInfo class describes the normalTextureInfo property of a glTF file.
     */
    class normalTextureInfo : textureInfo {
    public:
        normalTextureInfo(const std::string &source, const std::string &id); //!< The default constructor.

        /**
         * @brief from_json parses the information from the glTF file.
         * @param doc is the JSON to be parsed.
         */
        void from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) override;

        float scale = 1.0F;
    };

    /**
     * @brief The occlusionTextureInfo class describes the occlusionTextureInfo property of a glTF file.
     */
    class occlusionTextureInfo : textureInfo {
    public:
        occlusionTextureInfo(const std::string &source, const std::string &id); //!< The default constructor.

        /**
         * @brief from_json parses the information from the glTF file.
         * @param doc is the JSON to be parsed.
         */
        void from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) override;

        float strength = 1.0F;
    };

    /**
     * @brief The pbrMetallicRoughness_t class describes the pbrMetallicRoughness property of a glTF file.
     */
    class pbrMetallicRoughness_t : gltf_property {
    public:
        pbrMetallicRoughness_t(const std::string &source, const std::string &id); //!< The default constructor.

        /**
         * @brief from_json parses the information from the glTF file.
         * @param doc is the JSON to be parsed.
         */
        void from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) override;

        std::optional<std::array<float, 4>> baseColorFactor;
        std::unique_ptr<textureInfo>        baseColorTexture = nullptr;
        float                               metallicFactor   = 1;
        float                               roughnessFactor  = 1;
        rapidjson::Document                 metallicRoughnessTexture;
    };

    material(const std::string &source, const std::string &id); //!< The default constructor.

    /**
     * @brief from_json parses the information from the glTF file.
     * @param doc is the JSON to be parsed.
     */
    void from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) override;

    std::optional<std::string>              name;
    std::shared_ptr<pbrMetallicRoughness_t> pbrMetallicRoughness = nullptr;
    std::shared_ptr<normalTextureInfo>      normalTexture        = nullptr;
    std::shared_ptr<occlusionTextureInfo>   occlusionTexture     = nullptr;
    std::optional<textureInfo>              emissiveTexture;
    std::array<float, 3>                    emissiveFactor = {0.0F, 0.0F, 0.0F};
    std::optional<std::string>              alphaMode;
    std::optional<float>                    alphaCutoff;
    bool                                    doubleSided = false;
};
} // namespace gleb
#endif // GLEB_MATERIAL_HPP
