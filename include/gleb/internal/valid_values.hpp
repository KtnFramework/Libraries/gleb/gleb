/**
 * @file valid_values.hpp
 * @brief This file contains the valid values of various elements in a glTF file.
 */

#ifndef GLEB_INTERNAL_VALID_VALUES_HPP
#define GLEB_INTERNAL_VALID_VALUES_HPP
#include <array>
#include <cstdint>
#include <string>

namespace gleb {
constexpr size_t minimum_byteStride(4); //!< The minimum byte stride.

constexpr size_t maximum_byteStride(252); //!< The maximum byte stride.

constexpr size_t number_of_elements_matrix4x4(16); //!< The number of elements in a 4x4 matrix.

constexpr std::array<size_t, 6> valid_accessor_component_types{
    5120, // BYTE
    5121, // UNSIGNED_BYTE
    5122, // SHORT
    5123, // UNSIGNED_SHORT
    5125, // UNSIGNED_INT
    5126 // FLOAT
}; //!< The list of valid accessor component types.

constexpr std::array<uint8_t, 6> valid_accessor_max_min_lengths{
    1, 2, 3, 4, 9, 16 //
}; //!< The list of valid lengths of an accessor's max/min property.

const inline static std::array<std::string, 7> valid_accessor_types{
    "SCALAR",
    "VEC2",
    "VEC3",
    "VEC4",
    "MAT2",
    "MAT3",
    "MAT4" //
}; //!< The list of valid accessor types.

const inline static std::array<std::string, 4> valid_animation_target_paths{
    "translation", "rotation", "scale", "weights" //
}; //!< The list of valid animation target paths.

const inline static std::array<std::string, 3> valid_animation_sampler_interpolations{
    "LINEAR", "STEP", "CUBICSPLINE" //
}; //!< The list of valid animation sampler interpolation algorithms.

constexpr std::array<size_t, 3> valid_sparse_accessor_indices_component_types{
    5121, // UNSIGNED_BYTE
    5123, // UNSIGNED_SHORT
    5125 // UNSIGNED_INT
}; //!< The list of valid sparse accessor indices' component types.

const inline static std::array<std::string, 2> valid_camera_types{
    "perspective", "orthographic" //
}; //!< The list of valid camera types.

const inline static std::array<std::string, 2> valid_image_mimeTypes{
    "image/jpeg", "image/png" //
}; //!< The list of valid image MIME types.

const inline static std::array<std::string, 3> valid_material_alphaModes{
    "OPAQUE", "MASK", "BLEND" //
}; //!< The list of valid material alpha modes.

constexpr std::array<size_t, 7> valid_mesh_primitive_modes{
    0, // POINTS
    1, // LINES
    2, // LINE_LOOP
    3, // LINE_STRIP
    4, // TRIANGLES
    5, // TRIANGLE_STRIP
    6 // TRIANGLE_FAN
}; //!< The list of valid mesh primitive modes.

constexpr std::array<size_t, 2> valid_sampler_magFilters{
    9728, // NEAREST
    9729 // LINEAR
}; //!< The list of valid sampler magnification filters.

constexpr std::array<size_t, 6> valid_sampler_minFilters{
    9728, // NEAREST
    9729, // LINEAR
    9984, // NEAREST_MIPMAP_NEAREST
    9985, // LINEAR_MIPMAP_NEAREST
    9986, // NEAREST_MIPMAP_LINEAR
    9987 // LINEAR_MIPMAP_LINEAR
}; //!< The list of valid sampler minification filters.

constexpr std::array<size_t, 3> valid_sampler_wraps{
    33071, // CLAMP_TO_EDGE
    33648, // MIRRORED_REPEAT
    10497 // REPEAT
}; //!< The list of valid sampler wrap modes.
} // namespace gleb
#endif // GLEB_INTERNAL_VALID_VALUES_HPP
