/**
 * @file camera_orthographic.hpp
 * @brief This file contains the declaration of the camera_orthographic class.
 */

#ifndef GLEB_CAMERA_ORTHOGRAPHIC_HPP
#define GLEB_CAMERA_ORTHOGRAPHIC_HPP
#include "internal/gltf_property.hpp"

#include <optional>
#include <vector>

namespace gleb {
/**
 * @brief The camera_orthographic class describes the orthographic camera property of a glTF file.
 */
class camera_orthographic : gltf_property {
public:
    camera_orthographic(const std::string &source, const std::string &id); //!< The default constructor.

    /**
     * @brief from_json parses the information from the glTF file.
     * @param doc is the JSON to be parsed.
     */
    void from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) override;

    float xmag{}; // gltf_required
    float ymag{}; // gltf_required
    float znear{}; // gltf_required
    float zfar{}; // gltf_required
};
} // namespace gleb
#endif // GLEB_CAMERA_ORTHOGRAPHIC_HPP
