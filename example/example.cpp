#include <gleb/gltf.hpp>

#include <glm/vec3.hpp>

using std::cout;
using std::endl;

int main() {
    std::string gltf_monkey_source("resources/models/monkey.gltf");
    cout << "Reading from " << gltf_monkey_source << "..." << endl;

    gleb::glTF gltf_monkey(gltf_monkey_source);
    gltf_monkey.read();

    size_t accessor_position = gltf_monkey.meshes[0].primitives[0].attributes["POSITION"].GetUint();
    cout << "The accessor for the buffer for vertex positions is " << accessor_position << "." << endl;

    size_t count_position      = gltf_monkey.accessors[accessor_position].count / 3;
    size_t bufferView_position = gltf_monkey.accessors[accessor_position].bufferView.value();
    size_t byteLength_position = gltf_monkey.bufferViews[bufferView_position].byteLength;
    size_t offset_position = gltf_monkey.bufferViews[bufferView_position].byteOffset.value_or(gleb::default_byteOffset);

    //    auto *positions = (float *)malloc(byteLength_position);
    auto positions = std::make_unique<float[]>(byteLength_position);
    memcpy(positions.get(), &gltf_monkey.buffers[0].data().at(offset_position), byteLength_position);
    cout << "Vertex count: " << count_position << endl;
    //    for (int i = 0; i < count_position; ++i) { //
    //        cout << i << ": " //
    //             << *(positions.get() + i) << ", " //
    //             << *(positions.get() + i + 1) << ", " //
    //             << *(positions.get() + i + 2) << endl;
    //    }
}
