#include <gleb/internal/extras.hpp>

namespace gleb {
using namespace rapidjson;

extras_t::extras_t(const std::string &source, const std::string &id) : property_base_t(source, id) {}
extras_t::~extras_t() = default;

void extras_t::from_json(const GenericValue<UTF8<>> &doc) {
    Data.CopyFrom(doc, Data.GetAllocator());
}
} // namespace gleb
