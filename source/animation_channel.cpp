#define USE_GLEB_UTILS
#include <gleb/animation_channel.hpp>

#include <gleb/internal/default_values.hpp>
#include <gleb/internal/utils.hpp>
#include <gleb/internal/valid_values.hpp>

#include <optional>

namespace gleb {
using namespace rapidjson;

animation_channel::animation_channel(const std::string &source, const std::string &id)
    : gltf_property(source, id), target(source, id + ".target") {}

void animation_channel::from_json(const GenericValue<UTF8<>> &doc) {
    AssertExistence(doc, "sampler", kNumberType);
    AssertExistence(doc, "target", kObjectType);

    sampler = doc["sampler"].GetUint();
    target.from_json(doc["target"]);

    check_and_read_extensions(doc);
    check_and_read_extras(doc);
}
} // namespace gleb
