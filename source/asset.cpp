#define USE_GLEB_UTILS
#include <gleb/asset.hpp>

#include <gleb/internal/utils.hpp>

namespace gleb {
using namespace rapidjson;

asset_t::asset_t(const std::string &source, const std::string &id) : gltf_property(source, id) {}

void asset_t::from_json(const GenericValue<UTF8<>> &doc) {
    AssertExistence(doc, "version", kStringType);
    bool copyrightExists  = check_existence(doc, "copyright", kStringType);
    bool generatorExists  = check_existence(doc, "generator", kStringType);
    bool minVersionExists = check_existence(doc, "minVersion", kStringType);

    version = doc["version"].GetString();
    if (copyrightExists) { copyright = doc["copyright"].GetString(); }
    if (generatorExists) { generator = doc["generator"].GetString(); }
    if (minVersionExists) { minVersion = doc["minVersion"].GetString(); }

    check_and_read_extensions(doc);
    check_and_read_extras(doc);
}
} // namespace gleb
#undef USE_GLEB_UTILS
