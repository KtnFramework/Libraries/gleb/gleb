#define USE_GLEB_UTILS
#include <gleb/mesh.hpp>

#include <gleb/internal/default_values.hpp>
#include <gleb/internal/utils.hpp>
#include <gleb/internal/valid_values.hpp>

namespace gleb {
using namespace rapidjson;
using namespace std;

mesh::mesh(const string &source, const string &id) : gltf_property(source, id) {}

void mesh::from_json(const GenericValue<UTF8<>> &doc) {
    AssertExistence(doc, "primitives", kArrayType);
    primitives.clear();
    for (SizeType i = 0; i < doc["primitives"].GetArray().Size(); ++i) {
        mesh_primitive temp(Source, Identifier + ".primitives[" + to_string(i) + "]");
        temp.from_json(doc["primitives"][i]);
        primitives.emplace_back(temp);
    }

    weights.clear();
    if (check_existence(doc, "weights", kArrayType)) {
        for (SizeType i = 0; i < doc["weights"].GetArray().Size(); ++i) {
            if (!doc["weights"][i].IsNumber()) {
                FileReadError(Source, Identifier, "The value in weights[" + to_string(i) + "] is not a number.");
            }
            weights.emplace_back(doc["weights"][i].GetFloat());
        }
    }

    if (check_existence(doc, "name", kStringType)) { name = doc["name"].GetString(); }

    check_and_read_extensions(doc);
    check_and_read_extras(doc);
}
} // namespace gleb
#undef USE_GLEB_UTILS
