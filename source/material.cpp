#define USE_GLEB_UTILS
#include <gleb/material.hpp>

#include <gleb/internal/default_values.hpp>
#include <gleb/internal/utils.hpp>
#include <gleb/internal/valid_values.hpp>

#include <algorithm>

namespace gleb {
using namespace rapidjson;
using normalTextureInfo      = material::normalTextureInfo;
using occlusionTextureInfo   = material::occlusionTextureInfo;
using pbrMetallicRoughness_t = material::pbrMetallicRoughness_t;

normalTextureInfo::normalTextureInfo(const std::string &source, const std::string &id) : textureInfo(source, id) {}

void normalTextureInfo::from_json(const GenericValue<UTF8<>> &doc) {
    textureInfo::from_json(doc);
    if (check_existence(doc, "scale", kNumberType)) { scale = doc["scale"].GetFloat(); }
}

occlusionTextureInfo::occlusionTextureInfo(const std::string &source, const std::string &id)
    : textureInfo(source, id) {}

void occlusionTextureInfo::from_json(const GenericValue<UTF8<>> &doc) {
    textureInfo::from_json(doc);
    if (check_existence(doc, "strength", kNumberType)) { strength = doc["strength"].GetFloat(); }
}

pbrMetallicRoughness_t::pbrMetallicRoughness_t(const std::string &source, const std::string &id)
    : gltf_property(source, id) {}

void pbrMetallicRoughness_t::from_json(const GenericValue<UTF8<>> &doc) {
    if (check_existence(doc, "baseColorFactor", kArrayType)) {
        if (doc["baseColorFactor"].GetArray().Size() != 4) {
            FileReadError(Source, Identifier, "baseColorFactor must be an array of size 4.");
        } else {
            for (uint8_t i = 0; i < 4; ++i) {
                if (!doc["baseColorFactor"][i].IsNumber()) {
                    FileReadError(Source, Identifier, "Elements of baseColorFactor must be numbers.");
                } else {
                    baseColorFactor               = std::array<float, 4>();
                    baseColorFactor.value().at(i) = doc["baseColorFactor"][i].GetFloat();
                    if (baseColorFactor.value().at(i) < 0 || baseColorFactor.value().at(i) > 1) {
                        FileReadError(Source, Identifier, "Invalid base color factor(s).");
                    }
                }
            }
        }
    } else {
        baseColorFactor = default_material_pbrMetallicRoughness_baseColorFactor;
    }

    if (check_existence(doc, "baseColorTexture", kObjectType)) {
        baseColorTexture = std::make_unique<textureInfo>(Source, Identifier + ".baseColorTexture");
        baseColorTexture->from_json(doc["baseColorTexture"]);
    }

    if (check_existence(doc, "metallicFactor", kNumberType)) {
        metallicFactor = doc["metallicFactor"].GetFloat();
        metallicFactor = std::clamp(metallicFactor, 0.0F, 1.0F);
    }

    if (check_existence(doc, "roughnessFactor", kNumberType)) {
        roughnessFactor = doc["roughnessFactor"].GetFloat();
        roughnessFactor = std::clamp(roughnessFactor, 0.0F, 1.0F);
    }

    if (check_existence(doc, "metallicRoughnessTexture", kObjectType)) {
        metallicRoughnessTexture.CopyFrom(doc["metallicRoughnessTexture"], metallicRoughnessTexture.GetAllocator());
    }

    check_and_read_extensions(doc);
    check_and_read_extras(doc);
}

material::material(const std::string &source, const std::string &id) : gltf_property(source, id) {}

void material::from_json(const GenericValue<UTF8<>> &doc) {
    bool nameExists                 = check_existence(doc, "name", kStringType);
    bool pbrMetallicRoughnessExists = check_existence(doc, "pbrMetallicRoughness", kObjectType);
    bool normalTextureExists        = check_existence(doc, "normalTexture", kObjectType);
    bool occlusionTextureExists     = check_existence(doc, "occlusionTexture", kObjectType);
    bool emissiveTextureExists      = check_existence(doc, "emissiveTexture", kObjectType);
    bool emissiveFactorExists       = check_existence_array(doc, "emissiveFactor");
    bool alphaModeExists            = check_existence(doc, "alphaMode", kStringType);
    bool alphaCutoffExists          = check_existence(doc, "alphaCutoff", kNumberType);
    bool doubleSidedExists          = check_existence_bool(doc, "doubleSided");

    if (nameExists) { name = doc["name"].GetString(); }

    if (pbrMetallicRoughnessExists) {
        pbrMetallicRoughness.reset();
        pbrMetallicRoughness = std::make_shared<pbrMetallicRoughness_t>(Source, Identifier + ".pbrMetallicRoughness");
        pbrMetallicRoughness->from_json(doc["pbrMetallicRoughness"]);
    }

    if (normalTextureExists) {
        normalTexture.reset();
        normalTexture = std::make_shared<normalTextureInfo>(Source, Identifier + ".normalTexture");
        normalTexture->from_json(doc);
    }

    if (occlusionTextureExists) {
        occlusionTexture.reset();
        occlusionTexture = std::make_shared<occlusionTextureInfo>(Source, Identifier + ".occlusionTexture");
        occlusionTexture->from_json(doc);
    }

    if (emissiveTextureExists) { emissiveTexture->from_json(doc["emissiveTexture"]); }

    if (emissiveFactorExists) {
        if (doc["emissiveFactor"].GetArray().Size() != 3) {
            FileReadError(Source, Identifier, "emissiveFactor must be an array of size 3.");
        } else {
            for (uint8_t i = 0; i < 3; ++i) {
                if (!doc["emissiveFactor"][i].IsNumber()) {
                    FileReadError(Source, Identifier, "Elements of emissiveFactor must be numbers.");
                } else {
                    emissiveFactor.at(i) = doc["emissiveFactor"][i].GetFloat();
                    if (emissiveFactor.at(i) < 0 || emissiveFactor.at(i) > 1) {
                        FileReadError(Source, Identifier, "Invalid emissive factor(s).");
                    }
                }
            }
        }
    }

    if (alphaModeExists) {
        alphaMode = doc["alphaMode"].GetString();
        if (!check_validity(alphaMode.value(), valid_material_alphaModes)) {
            FileReadError(Source, Identifier, "Invalid alphaMode.");
        }
    } else {
        alphaMode = default_material_alphaMode;
    }

    if (alphaCutoffExists) {
        alphaCutoff = doc["alphaCutoff"].GetFloat();
    } else {
        alphaCutoff = default_alphaCutoff;
    }

    if (doubleSidedExists) { doubleSided = doc["doubleSided"].GetBool(); }

    check_and_read_extensions(doc);
    check_and_read_extras(doc);
}
} // namespace gleb
#undef USE_GLEB_UTILS
