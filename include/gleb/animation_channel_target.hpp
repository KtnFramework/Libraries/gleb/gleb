/**
 * @file animation_channel_target.hpp
 * @brief This file contains the declaration of the animation_channel_target class.
 */

#ifndef GLEB_ANIMATION_CHANNEL_TARGET_HPP
#define GLEB_ANIMATION_CHANNEL_TARGET_HPP
#include "internal/gltf_property.hpp"

#include <optional>

namespace gleb {
/**
 * @brief The animation_channel_target class describes the animation channel target property of a glTF file.
 */
class animation_channel_target : gltf_property {
public:
    animation_channel_target(const std::string &source, const std::string &id);

    /**
     * @brief from_json parses the information from the glTF file.
     * @param doc is the JSON to be parsed.
     */
    void from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) override;

    std::optional<size_t> node;
    std::string           path;
};
} // namespace gleb
#endif // GLEB_ANIMATION_CHANNEL_TARGET_HPP
