#define USE_GLEB_UTILS
#include <gleb/sparse_accessor_values.hpp>

#include <gleb/internal/default_values.hpp>
#include <gleb/internal/utils.hpp>
#include <gleb/internal/valid_values.hpp>

namespace gleb {
using namespace rapidjson;

sparse_accessor_values::sparse_accessor_values(const std::string &source, const std::string &id)
    : gltf_property(source, id) {}

void sparse_accessor_values::from_json(const GenericValue<UTF8<>> &doc) {
    AssertExistence(doc, "bufferView", kNumberType);
    bufferView = doc["bufferView"].GetUint();

    if (check_existence(doc, "byteOffset", kNumberType)) {
        byteOffset = doc["byteOffset"].GetUint();
    } else {
        byteOffset = default_byteOffset;
    }
    check_and_read_extensions(doc);
    check_and_read_extras(doc);
}
} // namespace gleb
