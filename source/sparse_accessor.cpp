#define USE_GLEB_UTILS
#include <gleb/sparse_accessor.hpp>

#include <gleb/internal/utils.hpp>

namespace gleb {
using namespace rapidjson;

sparse_accessor::sparse_accessor(const std::string &source, const std::string &id)
    : gltf_property(source, id),
      indices(sparse_accessor_indices(Source, Identifier + ".indices")),
      values(sparse_accessor_values(Source, Identifier + ".values")) {}

void sparse_accessor::from_json(const GenericValue<UTF8<>> &doc) {
    AssertExistence(doc, "count", kNumberType);
    AssertExistence(doc, "indices", kObjectType);
    AssertExistence(doc, "values", kObjectType);

    count = doc["count"].GetUint();
    indices.from_json(doc["indices"]);
    values.from_json(doc["values"]);
    check_and_read_extensions(doc);
    check_and_read_extras(doc);
}
} // namespace gleb
