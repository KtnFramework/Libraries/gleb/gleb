/**
 * @file sparse_accessor_indices.hpp
 * @brief This file contains the declaration of the sparse_accessor_indices class.
 */

#ifndef GLEB_SPARSE_ACCESSOR_INDICES_HPP
#define GLEB_SPARSE_ACCESSOR_INDICES_HPP
#include "internal/gltf_property.hpp"

#include <optional>

namespace gleb {
/**
 * @brief The sparse_accessor_indices class describes the sparse accessor indices property of a glTF file.
 */
class sparse_accessor_indices : gltf_property {
public:
    sparse_accessor_indices(const std::string &source, const std::string &id); //!< The default constructor.

    /**
     * @brief from_json parses the information from the glTF file.
     * @param doc is the JSON to be parsed.
     */
    void from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) override;

    size_t                bufferView{}; //!< The index of the associated bufferView;
    std::optional<size_t> byteOffset; //!< The optional byte offset.
    size_t componentType{}; //!< The type of the component stored in the buffer that the bufferView points to.
};
} // namespace gleb
#endif // GLEB_SPARSE_ACCESSOR_INDICES_HPP
