/**
 * @file property_base.hpp
 * @brief This file contains the declaration of the property_base_t class.
 */

#ifndef GLEB_INTERNAL_PROPERTY_BASE_HPP
#define GLEB_INTERNAL_PROPERTY_BASE_HPP
#include <rapidjson/document.h>

#include <string>

/**
 * @namespace gleb contains all contents of the gleb library.
 */
namespace gleb {
/**
 * @brief The property_base_t class describes the base class for all glTF properties.
 */
class property_base_t {
public:
    property_base_t(const std::string &source, const std::string &id);
    virtual ~property_base_t();

    /**
     * @brief from_json parses the information from the glTF file.
     * @param doc is the JSON to be parsed.
     */
    virtual void from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) = 0;
    // virtual void to_json() = 0;

    const std::string Identifier;
    const std::string Source;
};
} // namespace gleb
#endif // GLEB_INTERNAL_PROPERTY_BASE_HPP
