/**
 * @file image.hpp
 * @brief This file contains the declaration of the image class.
 */

#ifndef GLEB_IMAGE_HPP
#define GLEB_IMAGE_HPP
#include "internal/gltf_property.hpp"

#include <optional>
#include <vector>

namespace gleb {
/**
 * @brief The image class describes the image property of a glTF file.
 */
class image : gltf_property {
public:
    image(const std::string &source, const std::string &id); //!< The default constructor.

    /**
     * @brief from_json parses the information from the glTF file.
     * @param doc is the JSON to be parsed.
     */
    void from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) override;

    std::optional<std::string> uri; //!< The optional uri of the image.
    std::optional<std::string> mimeType; //!< The optional MIME type of the image.
    std::optional<size_t>      bufferView; //!< The index of the optional bufferView.
    std::optional<std::string> name; //!< The optional name of the image.
};
} // namespace gleb
#endif // GLEB_IMAGE_HPP
