/**
 * @file extras.hpp
 * @brief This file contains the declaration of the extras_t class.
 */

#ifndef GLEB_INTERNAL_EXTRAS_HPP
#define GLEB_INTERNAL_EXTRAS_HPP
#include "property_base.hpp"

namespace gleb {
/**
 * @brief The extras_t class describes the extras property of a glTF file.
 * Due to them being non-standard, they are to be parsed on a case-by-case basis in application code.
 */
class extras_t : property_base_t {
public:
    extras_t(const std::string &source, const std::string &id); //!< The default constructor.
    ~extras_t() override; //!< The destructor.

    /**
     * @brief from_json copy the extension object into \ref extras_t.Data.
     * @param doc is the JSON to be copied.
     */
    void from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) override;

    rapidjson::Document Data;
};
} // namespace gleb
#endif // GLEB_INTERNAL_EXTRAS_HPP
