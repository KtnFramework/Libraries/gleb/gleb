/**
 * @file accessor.hpp
 * @brief This file contains the declaration of the accessor class.
 */

#ifndef GLEB_ACCESSOR_HPP
#define GLEB_ACCESSOR_HPP
#include "internal/gltf_property.hpp"

#include "sparse_accessor.hpp"

#include <optional>
#include <utility>
#include <vector>

namespace gleb {
/**
 * @brief The accessor class describes the accessor property of a glTF file.
 */
class accessor : gltf_property {
public:
    accessor(const std::string &source, const std::string &id); //!< The default constructor.

    /**
     * @brief from_json parses the information from the glTF file.
     * @param doc is the JSON to be parsed.
     */
    void from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) override;

    std::optional<size_t>          bufferView;
    size_t                         byteOffset = 0;
    size_t                         componentType{}; // gltf_required
    bool                           normalized = false;
    size_t                         count{}; // gltf_required
    std::string                    type; // gltf_required
    std::vector<float>             max;
    std::vector<float>             min;
    std::optional<sparse_accessor> sparse;
    std::optional<std::string>     name;
};
} // namespace gleb
#endif // GLEB_ACCESSOR_HPP
