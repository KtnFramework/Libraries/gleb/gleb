#define USE_GLEB_UTILS
#include <gleb/node.hpp>

#include <gleb/internal/default_values.hpp>
#include <gleb/internal/utils.hpp>
#include <gleb/internal/valid_values.hpp>

namespace gleb {
using namespace rapidjson;

node::node(const std::string &source, const std::string &id) : gltf_property(source, id) {}

void node::from_json(const GenericValue<UTF8<>> &doc) {
    children.clear();
    weights.clear();

    bool cameraExists      = check_existence(doc, "camera", kNumberType);
    bool childrenExists    = check_existence(doc, "children", kArrayType);
    bool skinExists        = check_existence(doc, "skin", kNumberType);
    bool matrixExists      = check_existence(doc, "matrix", kArrayType);
    bool meshExists        = check_existence(doc, "mesh", kNumberType);
    bool rotationExists    = check_existence(doc, "rotation", kArrayType);
    bool scaleExists       = check_existence(doc, "scale", kArrayType);
    bool translationExists = check_existence(doc, "translation", kArrayType);
    bool weightsExists     = check_existence(doc, "weights", kArrayType);
    bool nameExists        = check_existence(doc, "name", kStringType);

    if (cameraExists) { camera = doc["camera"].GetUint(); }

    if (childrenExists) {
        for (auto &child : doc["children"].GetArray()) { children.emplace_back(child.GetUint()); }
    }

    if (skinExists) { skin = doc["skin"].GetUint(); }

    if (matrixExists) {
        matrix = default_node_matrix;
        if (doc["matrix"].GetArray().Size() != number_of_elements_matrix4x4) {
            FileReadError(Source, Identifier, "Invalid transformation matrix size.");
        }
        for (unsigned short i = 0; i < number_of_elements_matrix4x4; ++i) {
            matrix.value().at(i) = doc["matrix"][i].GetFloat();
        }
    }

    if (meshExists) { mesh = doc["mesh"].GetUint(); }

    if (rotationExists) {
        rotation = default_node_rotation;
        if (doc["rotation"].GetArray().Size() != 4) {
            FileReadError(Source, Identifier, "Invalid rotation array size.");
        }
        for (unsigned short i = 0; i < 4; ++i) { rotation.value().at(i) = doc["rotation"][i].GetFloat(); }
    }

    if (scaleExists) {
        scale = default_node_scale;
        if (doc["scale"].GetArray().Size() != 3) { FileReadError(Source, Identifier, "Invalid scale array size."); }
        for (unsigned short i = 0; i < 3; ++i) { scale.value().at(i) = doc["scale"][i].GetFloat(); }
    }

    if (translationExists) {
        translation = default_node_translation;
        if (doc["translation"].GetArray().Size() != 3) {
            FileReadError(Source, Identifier, "Invalid translation array size.");
        }
        for (unsigned short i = 0; i < 3; ++i) { translation.value().at(i) = doc["translation"][i].GetFloat(); }
    }

    if (weightsExists) {
        for (auto &weight : doc["weights"].GetArray()) { weights.emplace_back(weight.GetUint()); }
    }

    if (nameExists) { name = doc["name"].GetString(); }
}
} // namespace gleb
#undef USE_GLEB_UTILS
