#define USE_GLEB_UTILS
#include <gleb/image.hpp>

#include <gleb/internal/utils.hpp>
#include <gleb/internal/valid_values.hpp>

#include <algorithm>
#include <filesystem>

namespace gleb {
using namespace rapidjson;

image::image(const std::string &source, const std::string &id) : gltf_property(source, id) {}

void image::from_json(const GenericValue<UTF8<>> &doc) {
    if (check_existence(doc, "uri", kStringType)) { //
        std::filesystem::path p(Source);
        uri = p.remove_filename().string() + doc["uri"].GetString();
    }
    if (check_existence(doc, "mimeType", kStringType)) {
        mimeType = doc["mimeType"].GetString();
        if (valid_image_mimeTypes.end() ==
            std::find(valid_image_mimeTypes.begin(), valid_image_mimeTypes.end(), mimeType)) {
            FileReadError(Source, Identifier, "Invalid mime type.");
        }
    }
    if (check_existence(doc, "bufferView", kNumberType)) { bufferView = doc["bufferView"].GetUint(); }
    if (check_existence(doc, "name", kStringType)) { name = doc["name"].GetString(); }
    check_and_read_extensions(doc);
    check_and_read_extras(doc);
}
} // namespace gleb
#undef USE_GLEB_UTILS
