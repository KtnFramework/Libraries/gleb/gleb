#define USE_GLEB_UTILS
#include <gleb/buffer.hpp>

#include <gleb/internal/utils.hpp>

#include <filesystem>
#include <fstream>

namespace gleb {
using namespace rapidjson;
using namespace std;

buffer::buffer(const std::string &source, const std::string &id) : gltf_property(source, id) {}

void buffer::from_json(const GenericValue<UTF8<>> &doc) {
    AssertExistence(doc, "byteLength", kNumberType);
    bool uriExists  = check_existence(doc, "uri", kStringType);
    bool nameExists = check_existence(doc, "name", kStringType);

    byteLength = doc["byteLength"].GetUint();
    if (byteLength < 1) { ReportAndCrash("Byte length must be >= 1."); }
    if (uriExists) { uri = doc["uri"].GetString(); }
    if (nameExists) { name = doc["name"].GetString(); }
    check_and_read_extensions(doc);
    check_and_read_extras(doc);
}

vector<char> buffer::data(const size_t &starting_index, const size_t &length) const {
    if (starting_index + length >= m_Data.size() || length == SIZE_MAX) {
        return vector<char>(m_Data.begin() + starting_index, m_Data.end());
    }

    if (starting_index > m_Data.size()) { return vector<char>{}; }

    return vector<char>(m_Data.begin() + starting_index, m_Data.begin() + starting_index + length);
}

bool buffer::data_loaded() {
    return m_IsLoaded;
}

void buffer::load_data() {
    // load data from .bin file or embedded buffer
    if (uri.has_value()) {
        if (uri->length() < 4) { throw invalid_argument("The uri \"" + uri.value() + "\" is too short."); }

        if (uri->substr(uri.value().size() - 4, 4) == ".bin") {
            std::filesystem::path   parent_path = std::filesystem::path(Source).parent_path();
            std::ifstream           stream(parent_path.string() + "/" + uri.value(), std::ios::binary | std::ios::ate);
            std::ifstream::pos_type pos = stream.tellg();
            m_Data.resize(pos);

            stream.seekg(0, std::ios::beg);
            stream.read(&m_Data[0], pos);
        } else {
            string embedded_buffer_signature("data:application/octet-stream;base64,");
            if (uri->size() <= embedded_buffer_signature.size()) {
                throw invalid_argument("The uri \"" + uri.value() + "\" is too short for it to be an embedded buffer");
            }
            if (uri.value().substr(0, embedded_buffer_signature.size()) == embedded_buffer_signature) {
                // TODO: enable loading embedded buffer
                throw invalid_argument(
                    LocationReport(__FILE__, __LINE__, "Loading embedded buffers is currently not supported."));
            }
        }
    }
    m_IsLoaded = true;
} // namespace gltf_properties

void buffer::unload_data() {
    m_IsLoaded = false;
    // TODO: write the unload function to erase data
}
} // namespace gleb
#undef USE_GLEB_UTILS
