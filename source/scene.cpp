#define USE_GLEB_UTILS
#include <gleb/internal/utils.hpp>
#include <gleb/scene.hpp>

namespace gleb {
using namespace rapidjson;

scene_t::scene_t(const std::string &source, const std::string &id) : gltf_property(source, id) {}

void scene_t::from_json(const GenericValue<UTF8<>> &doc) {
    if (check_existence(doc, "nodes", kArrayType)) {
        nodes.clear();
        for (auto &node : doc["nodes"].GetArray()) { nodes.emplace_back(node.GetUint()); }
    }

    if (check_existence(doc, "name", kStringType)) { name = doc["name"].GetString(); }

    check_and_read_extensions(doc);
    check_and_read_extras(doc);
}
} // namespace gleb
#undef USE_GLEB_UTILS
