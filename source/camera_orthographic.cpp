#define USE_GLEB_UTILS
#include <gleb/camera_orthographic.hpp>

#include <gleb/internal/utils.hpp>
#include <gleb/internal/valid_values.hpp>

namespace gleb {
using namespace rapidjson;

camera_orthographic::camera_orthographic(const std::string &source, const std::string &id)
    : gltf_property(source, id) {}

void camera_orthographic::from_json(const GenericValue<UTF8<>> &doc) {
    AssertExistence(doc, "xmag", kNumberType);
    AssertExistence(doc, "ymag", kNumberType);
    AssertExistence(doc, "zfar", kNumberType);
    AssertExistence(doc, "znear", kNumberType);

    xmag  = doc["xmag"].GetFloat();
    ymag  = doc["ymag"].GetFloat();
    zfar  = doc["zfar"].GetFloat();
    znear = doc["znear"].GetFloat();
    if (zfar <= 0) { FileReadError(Source, Identifier, "Invalid zfar value."); }
    if (znear < 0) { FileReadError(Source, Identifier, "Invalid znear value."); }

    check_and_read_extensions(doc);
    check_and_read_extras(doc);
}
} // namespace gleb
