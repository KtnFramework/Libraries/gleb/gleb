/**
 * @file default_values.hpp
 * @brief This file contains the default values of various elements in a glTF file.
 */

#ifndef GLEB_INTERNAL_DEFAULT_VALUES_HPP
#define GLEB_INTERNAL_DEFAULT_VALUES_HPP
#include <array>
#include <optional>
#include <string>

namespace gleb {
constexpr float default_alphaCutoff(0.5F);

/**
 * @brief default_animation_sampler_interpolation is the default animation sampler interpolation algorithm.
 */
const inline static std::string default_animation_sampler_interpolation("LINEAR");

constexpr size_t default_byteOffset(0); //!< The default value of byteOffset entries.

constexpr std::array<float, 4> default_material_pbrMetallicRoughness_baseColorFactor{
    1.0F, 1.0F, 1.0F, 1.0F //
}; //!< The default PBR metallic/roughness base color factor.

const inline static std::string default_material_alphaMode("OPAQUE"); //!< The default material alpha mode.

constexpr size_t default_mesh_primitive_mode(4); //!< The default mesh primitive mode (TRIANGLES).

constexpr std::array<float, 16> default_node_matrix{
    1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1 //
}; //!< The default transformation matrix.

constexpr std::array<float, 4> default_node_rotation{
    0, 0, 0, 1 //
}; //!< The default rotation quaternion.

constexpr std::array<float, 3> default_node_scale{
    1, 1, 1 //
}; //!< The default scale vector.

constexpr std::array<float, 3> default_node_translation{
    0, 0, 0 //
}; //!< The default translation vector.

constexpr size_t default_sampler_wrapS(10497); //!< The default S wrapping mode.

constexpr size_t default_sampler_wrapT(10497); //!< The default T wrapping mode.
} // namespace gleb
#endif // GLEB_INTERNAL_DEFAULT_VALUES_HPP
