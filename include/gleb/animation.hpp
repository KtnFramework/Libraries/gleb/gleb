/**
 * @file animation.hpp
 * @brief This file contains the declaration of the animation class.
 */

#ifndef GLEB_ANIMATION_HPP
#define GLEB_ANIMATION_HPP
#include "internal/gltf_property.hpp"

#include "animation_channel.hpp"
#include "animation_sampler.hpp"

#include <vector>

namespace gleb {
/**
 * @brief The animation class describes the animation property of a glTF file.
 */
class animation : gltf_property {
public:
    animation(const std::string &source, const std::string &id); //!< The default constructor.

    /**
     * @brief from_json parses the information from the glTF file.
     * @param doc is the JSON to be parsed.
     */
    void from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) override;

    std::vector<animation_channel> channels; //!< The associated animation channels.
    std::vector<animation_sampler> samplers; //!< The associated animation samplers.
    std::optional<std::string>     name; //!< The optional name of the animation.
};
} // namespace gleb
#endif // GLEB_ANIMATION_HPP
