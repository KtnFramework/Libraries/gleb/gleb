/**
 * @file scene.hpp
 * @brief This file contains the declaration of the scene_t class.
 */

#ifndef GLEB_SCENE_HPP
#define GLEB_SCENE_HPP
#include "internal/gltf_property.hpp"

#include <optional>
#include <vector>

namespace gleb {
/**
 * @brief The scene_t class describes the scene property of a glTF file.
 */
class scene_t : gltf_property {
public:
    scene_t(const std::string &source, const std::string &id); //!< The default constructor.

    /**
     * @brief from_json parses the information from the glTF file.
     * @param doc is the JSON to be parsed.
     */
    void from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) override;

    std::vector<size_t>        nodes; //!< The indices of root nodes contained within this scene.
    std::optional<std::string> name; //!< The optional name of the scene.
};
} // namespace gleb
#endif // GLEB_SCENE_HPP
