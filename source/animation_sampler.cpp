#define USE_GLEB_UTILS
#include <gleb/animation_sampler.hpp>

#include <gleb/internal/default_values.hpp>
#include <gleb/internal/utils.hpp>
#include <gleb/internal/valid_values.hpp>

namespace gleb {
using namespace rapidjson;

animation_sampler::animation_sampler(const std::string &source, const std::string &id) : gltf_property(source, id) {}

void animation_sampler::from_json(const GenericValue<UTF8<>> &doc) {
    AssertExistence(doc, "input", kNumberType);
    AssertExistence(doc, "output", kNumberType);

    input  = doc["input"].GetUint();
    output = doc["output"].GetUint();

    if (check_existence(doc, "interpolation", kStringType)) {
        interpolation = doc["interpolation"].GetString();
        if (!check_validity(interpolation.value(), valid_animation_sampler_interpolations)) {
            FileReadError(Source, Identifier, "Invalid interpolation.");
        }
    } else {
        interpolation = default_animation_sampler_interpolation;
    }

    check_and_read_extensions(doc);
    check_and_read_extras(doc);
}
} // namespace gleb
